	Code adapted from projects on various sites
    Code based on
         https://www.instructables.com/Electronic-Affirmation-Mirror/
    Spiffs 
         https://tttapa.github.io/ESP8266/Chap11%20-%20SPIFFS.html
         https://www.instructables.com/Using-ESP8266-SPIFFS/
         https://nerdyelectronics.com/iot/esp8266/esp8266-spiffs-file-system/
	NTP Client 
        https://randomnerdtutorials.com/about/ 
    Time with No RTC
        NoRTC_ESP - https://www.instructables.com/TESTED-Timekeeping-on-ESP8266-Arduino-Uno-WITHOUT-/ - changed some of his to get it work but not bad for accuracy
	    
	Web Page 
        https://randomnerdtutorials.com/esp32-esp8266-input-data-html-form/ 
        https://lastminuteengineers.com/creating-esp8266-web-server-arduino-ide/
    Data Folder
        https://randomnerdtutorials.com/install-esp8266-filesystem-uploader-arduino-ide/ 
        https://diyprojects.io/esp8266-upload-data-folder-spiffs-littlefs-platformio/
