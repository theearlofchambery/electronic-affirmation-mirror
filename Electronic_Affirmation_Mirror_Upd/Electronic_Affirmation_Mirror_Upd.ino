/* 
	Code based on https://www.instructables.com/Electronic-Affirmation-Mirror/
	
	Features added:
	Web Page Message Update - One or all of the Messages can updated via a webpage
	Message Repeater- You can assign two messages that can be initiated by web buttons to override the random mode and repeat over and over
	Message Staging page- Allows you to stage a group of new messages allowing you to commit all at once you're ready to upload.
	Motion Activated  - using a PIR sensor 
	Sleep Mode - Stops the mirror from displaying messages between certain hours of the day
	/time webpage - displays the time on the mirror until you call up a different webpage or close the browser ?? need to test 
	
	
	Webpages
	/      - brings - up the messages edit page
	/stage - brings up Staging and commit 
	/night - brings up nightmode page
	/time  - brings up time display on mirror 
	/ver   - brings up current build version (notworking currently)
	Compiling Sketch:
	In the original code messages were stored in the sketch.  
	Messages, Staging, and config.txt files are now stored in the data folder of the same directory.  I installed the ESPTool and used that to upload the file to the Mirror, more information can be found at 	https://randomnerdtutorials.com/install-esp8266-filesystem-uploader-arduino-ide/
	
	Annoyances:
	Webpage is difficult to get to if motion is detected
	
	
	Code adapted from projects on various sites
	Code based on https://www.instructables.com/Electronic-Affirmation-Mirror/
	Spiffs - https://tttapa.github.io/ESP8266/Chap11%20-%20SPIFFS.html, https://www.instructables.com/Using-ESP8266-SPIFFS/, https://nerdyelectronics.com/iot/esp8266/esp8266-spiffs-file-system/,
	NTP Client - https://randomnerdtutorials.com/about/, - only synching once an hour
	NoRTC_ESP - https://www.instructables.com/TESTED-Timekeeping-on-ESP8266-Arduino-Uno-WITHOUT-/ - changed some of his to get it work but not bad for accuracy
	Web Page - https://randomnerdtutorials.com/esp32-esp8266-input-data-html-form/ , https://lastminuteengineers.com/creating-esp8266-web-server-arduino-ide/
	Data Folder - https://randomnerdtutorials.com/install-esp8266-filesystem-uploader-arduino-ide/ , https://diyprojects.io/esp8266-upload-data-folder-spiffs-littlefs-platformio/

	All code belongs to the respective owners and is not for sale.
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Tmp - added config array, and read to function.  
  changed counter for Staging to fix commit write to message.txt
  **Nightmode config  *
  *backup msgs
  *repeat
  
*/
// affirmation Mirror
//#include <iostream>
//#include <string>
#include <Wire.h>                  // installed by default
#include <SPI.h>
#include <Adafruit_GFX.h>          // https://github.com/adafruit/Adafruit-GFX-Library
#include <Adafruit_LEDBackpack.h>  // https://github.com/adafruit/Adafruit_LED_Backpack
// affirmation Mirror
#include <ESP8266WiFi.h>

#include <ESP8266WebServer.h>
#include <FS.h>   // Include the SPIFFS library
//NTP
#include <NTPClient.h>
#include <WiFiUdp.h>

/*Put your SSID & Password*/
//const char* ssid = "Your_SSID";  // Enter SSID here
//const char* password = "Your_Wifi";  //Enter Password here
const char* ssid = "";  // Enter SSID here
const char* password = "";  //Enter Password here    
									 
ESP8266WebServer server(80);    // Create a webserver object that listens for HTTP request on port 80

String getContentType(String filename); // convert the file extension to the MIME type
bool handleFileRead(String path);       // send the right file to the client (if it exists)
// affirmation mirror
int Status = 12;
int sensor = 13;
const int led = 2;
//NTP For UTC -5.00 : -5 * 60 * 60 : -18000
const long utcOffsetInSeconds = -18000;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");

String statusUpdate;
String Stage[21];				 
String Messages[20];
String Config[10];
String umsgs[20];
String smsgs[19];
String Smsg;
String upd_Msgs[20];
String upd_sMsgs[19];
String temp_Msgs[21];
String temp_sMsgs[19];
uint8_t LED1pin = 7; //Pin D7  Out from Sensor connects to this one
bool LED1status = LOW;
int bytesWritten;
uint8_t LED2pin = 6;  // Pin D6 not used
bool LED2status = LOW;

 // http://192.168.1.37/night?Night=True&Shut=23&Resume=6
 String Night;
 bool bNight=false;
 String Shut;
 String sShut;
 String Resume;
 String sResume;
 long iShut=0;
 long iResume=0;
 int nightarr[]={0,0,0,0};
 int currentHour;
 int currentMinute;
 unsigned long timeNow = 0;
 unsigned long timeLast = 0;

 int cShut;
 int cResume;

int seconds = 0;
int minutes = 0;
int hours = 0;

String Update1,Update2,Update3,Update4,Update5,Update6,Update7,Update8,Update9,Update10,Update11,Update12,Update13,Update14,Update15,Update16,Update17,Update18,Update19;
String stgupdate1,stgupdate2,stgupdate3,stgupdate4,stgupdate5,stgupdate6,stgupdate7,stgupdate8,stgupdate9,stgupdate10,stgupdate11,stgupdate12,stgupdate13,stgupdate14,stgupdate15,stgupdate16,stgupdate17,stgupdate18,stgupdate19;

//char displaybuffer[12] = {' ', ' ', ' ', ' ',' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
char displaybuffer[12];
struct { // label the alphanumeric displays with their i2c addresses
    uint8_t           addr;         // I2C address
    Adafruit_AlphaNum4 alpha4;         // 7segment object
	      } disp[] = {
	  { 0x72, Adafruit_AlphaNum4() },  // left-hand display
	  { 0x71, Adafruit_AlphaNum4() },  // middle display
	  { 0x70, Adafruit_AlphaNum4() },  // right-hand display

      };	

void handle_statusUpd () {
  if (timeNow == 0){
  String statusUpdate = "***  INITIALIZING ELECTRONIC AFFIRMATION MIRROR   ***            VER 16.2.0   ...            ";
  }
  else {
   String statusUpdate = "***  VER 16.2.0            ... POWERED BY 16AMPS            ***";
  }
  Serial.println("STATUSUPDATE");
   String displaySpecificMessage = statusUpdate;
      Serial.println(displaySpecificMessage);

      for (int i=0; i<displaySpecificMessage.length(); i++){
        
        // scroll down display
        displaybuffer[0] = displaybuffer[1];
        displaybuffer[1] = displaybuffer[2];
        displaybuffer[2] = displaybuffer[3];
        displaybuffer[3] = displaybuffer[4];
        displaybuffer[4] = displaybuffer[5];
        displaybuffer[5] = displaybuffer[6];
        displaybuffer[6] = displaybuffer[7];
        displaybuffer[7] = displaybuffer[8];
        displaybuffer[8] = displaybuffer[9];
        displaybuffer[9] = displaybuffer[10];
        displaybuffer[10] = displaybuffer[11];
        displaybuffer[11] = displaySpecificMessage.charAt(i);

        // set every digit to the buffer
        disp[0].alpha4.writeDigitAscii(0, displaybuffer[0]);
        disp[0].alpha4.writeDigitAscii(1, displaybuffer[1]);
        disp[0].alpha4.writeDigitAscii(2, displaybuffer[2]);
        disp[0].alpha4.writeDigitAscii(3, displaybuffer[3]);
        disp[1].alpha4.writeDigitAscii(0, displaybuffer[4]);
        disp[1].alpha4.writeDigitAscii(1, displaybuffer[5]);
        disp[1].alpha4.writeDigitAscii(2, displaybuffer[6]);
        disp[1].alpha4.writeDigitAscii(3, displaybuffer[7]);
        disp[2].alpha4.writeDigitAscii(0, displaybuffer[8]);
        disp[2].alpha4.writeDigitAscii(1, displaybuffer[9]);
        disp[2].alpha4.writeDigitAscii(2, displaybuffer[10]);
        disp[2].alpha4.writeDigitAscii(3, displaybuffer[11]);
       
        // write it out!
        disp[0].alpha4.writeDisplay();
        disp[1].alpha4.writeDisplay();
        disp[2].alpha4.writeDisplay();
        delay(200);
        }
     //   Return;
}

void Messaging () {
bool success = SPIFFS.begin();
   if (!success) {
    Serial.println("Error mounting the file system");
    return;
  }
  
  File msgfile = SPIFFS.open("/messages.txt", "r");
   if (!msgfile) {
    Serial.println("Failed to open file for reading");
    return;
  }
 
  Serial.println("Message File Content:");
  int i=0;
  while (msgfile.available()) {
      String msg=msgfile.readStringUntil('\r');  
      Serial.print(i);
      Serial.println('\n');
      //Serial.println(msg);
       if (i>0){ 
      Messages[i] = " " + msg + "            " ; 
//         Messages[i] = " " + msg; 
        i++;
	    } else {
      Messages[i] = msg ; 
      i++;
         }
      Serial.write(msgfile.read());
  }
 
  msgfile.close();
  
  for (int i=0; i<20; i++){
    Serial.println(i);
  Serial.println(Messages[i]);
    }
} 
    										
void Staging () {
// get staged
  bool success = SPIFFS.begin();
   if (!success) {
    Serial.println("Error mounting the file system");
    return;
  }
  
  File msgfile = SPIFFS.open("/Stage.txt", "r");
   if (!msgfile) {
    Serial.println("Failed to open file for reading");
    return;
  }
 
  Serial.println("Staging File Content:");
  int i=0;
  while (msgfile.available()) {
      String msg=msgfile.readStringUntil('\r');  
      Serial.print(i);
      Serial.println('\n');
      Serial.println(msg);
       if (i>0){ 
      Stage[i] = " " + msg + "            " ; 
//         Stage[i] = " " + msg; 
        i++;
      } else {
      Stage[i] = msg ; 
      i++;
         }
     // Serial.write(msgfile.read());
  }
   msgfile.close();
    return;
      }

// got staged 

 void config () {
bool success = SPIFFS.begin();
   if (!success) {
    Serial.println("Error mounting the file system");
    return;
  }
  
  File cfgfile = SPIFFS.open("/config.txt", "r");
   if (!cfgfile) {
    Serial.println("Failed to open file for reading");
    return;
  }
 
  Serial.println("Config File Content:");
  int i=0;
  while (cfgfile.available()) {
      String cfg=cfgfile.readStringUntil('\r');  
      Serial.print(i);
      Serial.println('\n');
      Serial.println(cfg);
       if (i>0){ 
      Config[i] = " " + cfg + "            " ; 
//         Config[i] = " " + cfg; 
        i++;
     } else {
      Config[i] = cfg ; 
      i++;
         }
      Serial.write(cfgfile.read());
  }
 
  cfgfile.close();
  
  for (int i=0; i<11; i++){
    Serial.println(i);
    Serial.println(Config[i]);
    
    }
  cShut =  Config[1].toInt();
  cResume = Config[2].toInt();
  nightarr[0] = cShut;
  Serial.println("cShut");
  Serial.println(cShut);
  nightarr[1] = cResume;
  Serial.println("cResume");
  Serial.println(cResume);
  bNight = Config[0];
    Serial.println("bNight");
  Serial.println(Config[0]);
}   
    void getTime (){
    timeClient.update();
    currentHour = timeClient.getHours();

    Serial.print("==getTime Output==");
    Serial.print("Hour: ");
    Serial.println(currentHour); 
    currentMinute = timeClient.getMinutes();

    Serial.print("Min: ");
    Serial.println(currentMinute); 
    return;
    }
    
  void setup() {
		   
   Serial.begin(115200);         // Start the Serial communication to send messages to the computer
  delay(10);
  
  Serial.println('\n');
   //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  Serial.println(WiFi.localIP());
  timeClient.begin();
  timeClient.setTimeOffset(-14400);
  config();
  Staging();
  Messaging();
  server.on("/", handle_OnConnect);
  server.on("/stage", handle_Onstage);
  server.on("/Stage", handle_Onstage);
  server.on("/stgupdate", handle_stgupdate);
  server.on("/led1on", handle_led1on);
  server.on("/led1off", handle_led1off);
  server.on("/led2on", handle_led2on);
  server.on("/led2off", handle_led2off);
  server.on("/update", handle_update);
  server.on("/night", handle_Nightmode);
  server.on("/commit", handle_commit);
  server.on("/SETnight", handle_SETnight);
  server.on("/time",  handle_showTime);
  server.on("/ver", handle_statusUpd);							   
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");
  
      // affirmation mirror
      pinMode(sensor, INPUT); // declare PIR sensor as input
      pinMode(Status, OUTPUT);  // declare Backpack as output
                                //   pinMode(led, OUTPUT);
 
          for(uint8_t i=0; i<3; i++) {       // Initialize 3 displays
          disp[i].alpha4.begin(disp[i].addr);
          disp[i].alpha4.clear();
          disp[i].alpha4.writeDisplay();
          }
 getTime(); 
// handle_statusUpd(); 
  } 

String SendHTML(uint8_t led1stat,uint8_t led2stat){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>Current Messages</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
 
  //ptr +=".button {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  ptr += ".button { display: inline-block; padding: 8px 20px; font-size: 15px; cursor: pointer; text-align: center; text-decoration: none;  outline: none; color: #fff; background-color: #555555; border: none;border-radius: 15px;box-shadow: 0 7px #999;}\n";
 
  ptr += ".button:hover {background-color: #0f4f84}\n";

  ptr +=".button-on {background-color: #555555;}\n"; // 1abc9c
  //ptr +=".button-on:active {background-color: #16a085;}\n";
  ptr +=".button-on:active {background-color: #34495e; box-shadow: 0 5px #666;  transform: translateY(4px);}\n";
  //ptr +=".button-off {background-color: #34495e;}\n";
  ptr +=".button-off {background-color: #3e8e41;}\n"; //3e8e41
  ptr +=".button-off:active {background-color: #3e8e41;}\n"; //2c3e50
  ptr +="p {font-size: 14px;color: #888;margin-bottom: 10px;}\n";

  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<h1>Electronic Affirmation Mirror Updater</h1>\n";
  ptr +="<h3>Current Messages</h3>\n";
 
   ptr +=" <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\">\n";
  ptr +=" <tr>\n";
   ptr +="   <td width=\"20%\"><br><p align=\"left\"> <a href=\"/stage\" align=\"left\" >Stage Messages</a><p align=\"left\"> <a href=\"/night\" align=\"left\" >NightMode</a>  </p><br>  </p> </td>\n";
    ptr +="  <td width=\"60%\">\n";
  ptr +="<h4>Enable Message Repeat</h4>\n";
   if(led1stat)
  {ptr +="<p>Repeat Message 18</p><a class=\"button button-off\" href=\"/led1off\">Active</a>\n";}
  else
      {ptr +="<p>Repeat Message 18</p><a class=\"button button-on\" href=\"/led1on\">OFF</a>\n";}
  if(led2stat)
      {ptr +="<p>Repeat Message 19</p><a class=\"button button-off\" href=\"/led2off\">Active</a>\n";}
  else
      {ptr +="<p>Repeat Message 19</p><a class=\"button button-on\" href=\"/led2on\">OFF</a>\n";}
  ptr +="  </td>\n";
   ptr +="   <td width=\"20%\">  </td>\n";
  ptr +=" </tr>\n";
ptr +="</table>\n";
  ptr +="<br><br>\n";
 String Sub = "<input type=\"submit\" value=\"Submit\">";
 String Faction = "<form action=\"/update\">";

     ptr +=  Faction;
      for (int i=1; i<20; i++) {
              
              String LabO1 =  "<label for=\"";
              String LabO2 = "\">";
              String MLabel = Messages[i];
              String LabC = "</label><input type=\"text\" id=\"";
              String NInput = "\" name=\"update";
              String CInput = "\">";
             			
              String FHtml =  LabO1 + i + LabO2 + MLabel + LabC + i + NInput + i + CInput;
              
              ptr += FHtml;
              ptr += "<br>";
             // Serial.println(FHtml);
      }
    String CFaction = "</form>" ;
        ptr += "<br>";
        ptr += "<br>";
        ptr += Sub;
        ptr += CFaction;
  String LabO1 =  "<label for=\"";          
	String Oform = "<form>";
	String Odiv = "<div>";
	String Cdiv = "</div>";
	String sbx = "Nightmode\">Enable Night Mode</label>";
	String chk = " <input type=\"checkbox\" id=\"Nightmode\" name=\"Night\" value=\"True\">";
	String Cbx = Oform + Odiv + chk + LabO1 + sbx + Cdiv + CFaction;
    //ptr += Cbx;
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}

// StageHtml
  String stageHTML(){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>Stage Your Messages</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
  ptr +="p {font-size: 14px;color: #888;margin-bottom: 10px;}\n";
  
  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<h1>Electronic Affirmation Mirror Stager</h1>\n";
  ptr +="<h3>Stage Your Messages </h3>\n";
  ptr +="<p align=\"left\"> <a href=\"/\" align=\"left\" >Messages</a>  </p><p align=\"left\"> <a href=\"/night\" align=\"left\" >NightMode</a>  </p>\n";
 String Sub = "<input type=\"submit\" value=\"Submit\">";
 String Faction = "<form action=\"/stgupdate\">";
    //Serial.println(Stage[5]);
     ptr +=  Faction;
      for (int i=1; i<18; i++) {
        //       Serial.println(Stage[i]);
              String LabO1 =  "<label for=\"";
              String LabO2 = "\">";
              String MLabel = Stage[i];
            //  Serial.println(MLabel);
              
              String LabC = "</label><input type=\"text\" id=\"";
              String NInput = "\" name=\"stgupdate";
              String CInput = "\">";
                   
              String FHtml =  LabO1 + i + LabO2 + MLabel + LabC + i + NInput + i + CInput;
              
              ptr += FHtml;
              ptr += "<br>";
       //       Serial.println(FHtml);
      }
    String CFaction = "</form>" ;
        ptr += "<br>";
        ptr += "<br>";
       // ptr += Sub;

  String LabO1 =  "<label for=\"";          
  String Oform = "<form>";
  String Odiv = "<div>";
  String Cdiv = "</div>";
  	
		
		ptr += "<input type=\"submit\" value=\"Save\"><br><br>\n";
		ptr += "<input type=\"submit\" value=\"Commit\" formaction=\"/commit\">\n<br><br>";
		/* ptr += "<label for=\"Random\">Randomize the Messages</label><br>\n";
    ptr += "<input type=\"checkbox\" id=\"Random\" name=\"Random\" value=\"True\">\n";
    */
  ptr += CFaction;
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
  }

// Stage HTML close 
String nightModeHTML () {
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>Electronic Affirmation</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
  ptr +="p {font-size: 14px;color: #888;margin-bottom: 10px;}\n";
  
  ptr +=".switch { position: relative;  display: inline-block; width: 60px; height: 34px;}";
  ptr +=".switch input { opacity: 0;  width: 0;  height: 0;}";
  ptr +=".slider { position: absolute;  cursor: pointer;  top: 0;  left: 0;  right: 0;  bottom: 0;  background-color: #ccc;  -webkit-transition: .4s;  transition: .4s;}";
  ptr +=".slider:before {position: absolute; content: \"\"; height: 26px; width: 26px;left: 4px;bottom: 4px;background-color: white; -webkit-transition: .4s; transition: .4s;}";
  ptr +="input:checked + .slider {background-color: #2196F3;}";
  ptr +="input:focus + .slider { box-shadow: 0 0 1px #2196F3;}";
  ptr +="input:checked + .slider:before { -webkit-transform: translateX(26px);  -ms-transform: translateX(26px);  transform: translateX(26px);}";
  //ptr +="\/* Rounded sliders *\/";
  ptr +=".slider.round { border-radius: 34px;}";
  ptr +=".slider.round:before { border-radius: 50%;}";
  
  ptr +="</style>\n";
  ptr +="</head>\n";

  ptr +="<body>\n";
  ptr +="<h1>Electronic Affirmation Mirror - Nightmode</h1>\n";
  // ptr +="<h3>Nightmode</h3>\n";
  ptr +="<p> This sets the time to shut of the the display, resume the display, and enable or disable nightmode (default) is off</p>\n";
  ptr +="<br><p align=\"left\"> <a href=\"/\" align=\"left\" >Messages</a>  </p><p align=\"left\"> <a href=\"/stage\" align=\"left\" >Stage Messages</a> </p><br>\n";
 
  ptr +="<form action=\"/SETnight\">";
  
  if (bNight == false ){
  ptr +="<label for=\"fname\">Shutoff at Hour: </label><input type=\"text\" id=\"shut\" name=\"Shut\"><br>\n";
  ptr +=" <br><label for=\"lname\">Resume at Hour:</label><input type=\"text\" id=\"Resume\" name=\"Resume\">\n";
  ptr +="<h4> Enable Nightmode </h4>\n";
  ptr += "<div><label class=\"switch\" for=\"Nightmode\"> <input type=\"checkbox\" id=\"Nightmode\" name=\"Night\" value=\"True\"><span class=\"slider round\"></span><br><br></label> </div><br>\n";
  ptr +="<br><input type=\"submit\" value=\"Submit\"></form>\n";
  } else {
    /*
               String LabC = "</label><input type=\"text\" id=\"";
              String NInput = "\" name=\"stgupdate";
              String CInput = "\">";
                   
              String FHtml =  LabO1 + i + LabO2 + MLabel + LabC + i + NInput + i + CInput;
  ptr +="<label for=\"fname\">Shutoff Hour: </label><input type=\"text\" id=\"shut\" name=\"Shut\" value=\" \"><br>\n";
  ptr +=" <br><label for=\"lname\">Resume Hour:</label><input type=\"text\" id=\"Resume\" name=\"Resume\">\n"; 
              */
             String nmSLab = "<label for=\"fname\">Shutoff at Hour: </label><input type=\"text\" id=\"shut\" name=\"Shut\" value=\"";
             String nmSCLab = "\"><br>\n";
             String nmRLab = "<br><label for=\"lname\">Resume at Hour:</label><input type=\"text\" id=\"Resume\" name=\"Resume\" value=\"";
             String nmRCLab = "\"><br>\n";
             
             ptr += nmSLab + nightarr[0] + nmSCLab + nmRLab + nightarr[1] + nmRCLab;
             ptr += "<h4> Enable Nightmode </h4>\n";
             ptr += "<div><label class=\"switch\" for=\"Nightmode\"> <input type=\"checkbox\" id=\"Nightmode\" name=\"Night\"";
             ptr += "value=\"" + Config[0] + "\" checked><span class=\"slider round\"></span><br><br></label> </div><br>\n";
             ptr += "<br><input type=\"submit\" value=\"Submit\"></form>\n";
  }
  //ptr +="<h4> Enable Nightmode </h4>\n";
  //ptr += "<div><label class=\"switch\" for=\"Nightmode\"> <input type=\"checkbox\" id=\"Nightmode\" name=\"Night\" value=\"True\"><span class=\"slider round\"></span><br><br></label> </div><br>\n";
 // ptr +="<br><input type=\"submit\" value=\"Submit\"></form>\n";
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}

void handle_Onstage() {
     Serial.println("Exit Stage Left");
     server.send(200, "text/html", stageHTML()); 
    }
  
void handleRoot();              // function prototypes for HTTP handlers

void handleNotFound();

void handle_OnConnect() {
  LED1status = LOW;
  LED2status = LOW;
  Serial.println("LJ Status: OFF | RP Status: OFF");
  server.send(200, "text/html", SendHTML(LED1status,LED2status)); 
}

void handle_led1on() {
  LED1status = HIGH;
  while (LED1status==HIGH) {
  server.handleClient();
  Serial.println("LJ Status: ON");
   String displaySpecificMessage = Messages[18];
			Serial.println(displaySpecificMessage);

			for (int i=0; i<displaySpecificMessage.length(); i++){
			  
			  // scroll down display
			  displaybuffer[0] = displaybuffer[1];
			  displaybuffer[1] = displaybuffer[2];
			  displaybuffer[2] = displaybuffer[3];
			  displaybuffer[3] = displaybuffer[4];
			  displaybuffer[4] = displaybuffer[5];
			  displaybuffer[5] = displaybuffer[6];
			  displaybuffer[6] = displaybuffer[7];
			  displaybuffer[7] = displaybuffer[8];
			  displaybuffer[8] = displaybuffer[9];
			  displaybuffer[9] = displaybuffer[10];
			  displaybuffer[10] = displaybuffer[11];
			  displaybuffer[11] = displaySpecificMessage.charAt(i);

			  // set every digit to the buffer
			  disp[0].alpha4.writeDigitAscii(0, displaybuffer[0]);
			  disp[0].alpha4.writeDigitAscii(1, displaybuffer[1]);
			  disp[0].alpha4.writeDigitAscii(2, displaybuffer[2]);
			  disp[0].alpha4.writeDigitAscii(3, displaybuffer[3]);
			  disp[1].alpha4.writeDigitAscii(0, displaybuffer[4]);
			  disp[1].alpha4.writeDigitAscii(1, displaybuffer[5]);
			  disp[1].alpha4.writeDigitAscii(2, displaybuffer[6]);
			  disp[1].alpha4.writeDigitAscii(3, displaybuffer[7]);
			  disp[2].alpha4.writeDigitAscii(0, displaybuffer[8]);
			  disp[2].alpha4.writeDigitAscii(1, displaybuffer[9]);
			  disp[2].alpha4.writeDigitAscii(2, displaybuffer[10]);
			  disp[2].alpha4.writeDigitAscii(3, displaybuffer[11]);
			 
			  // write it out!
			  disp[0].alpha4.writeDisplay();
			  disp[1].alpha4.writeDisplay();
			  disp[2].alpha4.writeDisplay();
			  delay(200);
			  }
		
    delay(150);
    server.send(200, "text/html", SendHTML(true,LED2status)); 
     };
  }
  
void notifymsgupd () {
    String updmsg = "***MESSAGE SUCCESSFULLY UPDATED***            ";

    Serial.println(updmsg);
   //   int specmsg = (updmsg.length() - 1);
   for (int i=0; i<updmsg.length(); i++){
  
  // scroll down display
  displaybuffer[0] = displaybuffer[1];
  displaybuffer[1] = displaybuffer[2];
  displaybuffer[2] = displaybuffer[3];
  displaybuffer[3] = displaybuffer[4];
  displaybuffer[4] = displaybuffer[5];
  displaybuffer[5] = displaybuffer[6];
  displaybuffer[6] = displaybuffer[7];
  displaybuffer[7] = displaybuffer[8];
  displaybuffer[8] = displaybuffer[9];
  displaybuffer[9] = displaybuffer[10];
  displaybuffer[10] = displaybuffer[11];
  displaybuffer[11] = updmsg.charAt(i);

  // set every digit to the buffer
  disp[0].alpha4.writeDigitAscii(0, displaybuffer[0]);
  disp[0].alpha4.writeDigitAscii(1, displaybuffer[1]);
  disp[0].alpha4.writeDigitAscii(2, displaybuffer[2]);
  disp[0].alpha4.writeDigitAscii(3, displaybuffer[3]);
  disp[1].alpha4.writeDigitAscii(0, displaybuffer[4]);
  disp[1].alpha4.writeDigitAscii(1, displaybuffer[5]);
  disp[1].alpha4.writeDigitAscii(2, displaybuffer[6]);
  disp[1].alpha4.writeDigitAscii(3, displaybuffer[7]);
  disp[2].alpha4.writeDigitAscii(0, displaybuffer[8]);
  disp[2].alpha4.writeDigitAscii(1, displaybuffer[9]);
  disp[2].alpha4.writeDigitAscii(2, displaybuffer[10]);
  disp[2].alpha4.writeDigitAscii(3, displaybuffer[11]);
 
  // write it out!
  disp[0].alpha4.writeDisplay();
  disp[1].alpha4.writeDisplay();
  disp[2].alpha4.writeDisplay();
  delay(150);
  }
  return;
}
//
void handle_showTime () {
    String Otime = "***       ";
    String btime = ":";
    String Ctime = "            ***       ";
   String tmsg= Otime + currentHour + btime + currentMinute + Ctime;

    Serial.println(tmsg);
   //   int specmsg = (tmsg.length() - 1);
   for (int i=0; i<tmsg.length(); i++){
  
  // scroll down display
  displaybuffer[0] = displaybuffer[1];
  displaybuffer[1] = displaybuffer[2];
  displaybuffer[2] = displaybuffer[3];
  displaybuffer[3] = displaybuffer[4];
  displaybuffer[4] = displaybuffer[5];
  displaybuffer[5] = displaybuffer[6];
  displaybuffer[6] = displaybuffer[7];
  displaybuffer[7] = displaybuffer[8];
  displaybuffer[8] = displaybuffer[9];
  displaybuffer[9] = displaybuffer[10];
  displaybuffer[10] = displaybuffer[11];
  displaybuffer[11] = tmsg.charAt(i);

  // set every digit to the buffer
  disp[0].alpha4.writeDigitAscii(0, displaybuffer[0]);
  disp[0].alpha4.writeDigitAscii(1, displaybuffer[1]);
  disp[0].alpha4.writeDigitAscii(2, displaybuffer[2]);
  disp[0].alpha4.writeDigitAscii(3, displaybuffer[3]);
  disp[1].alpha4.writeDigitAscii(0, displaybuffer[4]);
  disp[1].alpha4.writeDigitAscii(1, displaybuffer[5]);
  disp[1].alpha4.writeDigitAscii(2, displaybuffer[6]);
  disp[1].alpha4.writeDigitAscii(3, displaybuffer[7]);
  disp[2].alpha4.writeDigitAscii(0, displaybuffer[8]);
  disp[2].alpha4.writeDigitAscii(1, displaybuffer[9]);
  disp[2].alpha4.writeDigitAscii(2, displaybuffer[10]);
  disp[2].alpha4.writeDigitAscii(3, displaybuffer[11]);
 
  // write it out!
  disp[0].alpha4.writeDisplay();
  disp[1].alpha4.writeDisplay();
  disp[2].alpha4.writeDisplay();
  delay(150);
  }
  return;
}

//
void handle_led1off() {
  LED1status = LOW;
  Serial.println("LJ Status: OFF");
  server.send(200, "text/html", SendHTML(false,LED2status)); 
}

void handle_led2on() {
  LED2status = HIGH;
    while (LED2status==HIGH) {
  server.handleClient();
  Serial.println("RP Status: ON");
  String displaySpecificMessage = Messages[19];
			Serial.println(displaySpecificMessage);
      int specmsg = (displaySpecificMessage.length() - 1);
			for (int i=0; i<specmsg; i++){
			  
			  // scroll down display
			  displaybuffer[0] = displaybuffer[1];
			  displaybuffer[1] = displaybuffer[2];
			  displaybuffer[2] = displaybuffer[3];
			  displaybuffer[3] = displaybuffer[4];
			  displaybuffer[4] = displaybuffer[5];
			  displaybuffer[5] = displaybuffer[6];
			  displaybuffer[6] = displaybuffer[7];
			  displaybuffer[7] = displaybuffer[8];
			  displaybuffer[8] = displaybuffer[9];
			  displaybuffer[9] = displaybuffer[10];
			  displaybuffer[10] = displaybuffer[11];
			  displaybuffer[11] = displaySpecificMessage.charAt(i);

			  // set every digit to the buffer
			  disp[0].alpha4.writeDigitAscii(0, displaybuffer[0]);
			  disp[0].alpha4.writeDigitAscii(1, displaybuffer[1]);
			  disp[0].alpha4.writeDigitAscii(2, displaybuffer[2]);
			  disp[0].alpha4.writeDigitAscii(3, displaybuffer[3]);
			  disp[1].alpha4.writeDigitAscii(0, displaybuffer[4]);
			  disp[1].alpha4.writeDigitAscii(1, displaybuffer[5]);
			  disp[1].alpha4.writeDigitAscii(2, displaybuffer[6]);
			  disp[1].alpha4.writeDigitAscii(3, displaybuffer[7]);
			  disp[2].alpha4.writeDigitAscii(0, displaybuffer[8]);
			  disp[2].alpha4.writeDigitAscii(1, displaybuffer[9]);
			  disp[2].alpha4.writeDigitAscii(2, displaybuffer[10]);
			  disp[2].alpha4.writeDigitAscii(3, displaybuffer[11]);
			 
			  // write it out!
			  disp[0].alpha4.writeDisplay();
			  disp[1].alpha4.writeDisplay();
			  disp[2].alpha4.writeDisplay();
			  delay(200);
			  } 
  Serial.println("RP Status: ON");
  server.send(200, "text/html", SendHTML(LED1status,true)); 
    };
}

void handle_led2off() {
  LED2status = LOW;
  
  Serial.println("GPIO6 Status: OFF");
  server.send(200, "text/html", SendHTML(LED1status,false)); 
}

void handle_update() {
      //server.argName(i);     //Get the name of the parameter
      //server.arg(i) ;              //Get the value of the parameter
  Serial.println("Current Web Data if any");     
  Serial.println(server.arg(0)); // 
  if ((Update1)||(Update2)||(Update3)||(Update4)||(Update5)||(Update6)||(Update7)||(Update8)||(Update9)||(Update10)||(Update11)||(Update12)||(Update13)||(Update14)||(Update15)||(Update16)||(Update17)||(Update18)||(Update19));
  {
    String Pmsg ="<h1>Messages Updated!</h1> <p> Info successfully received</p>";
    for (int i = 0; i < server.args(); i++) {
          String umsg = server.arg(i);
                        // Serial.println(upd_Msgs[i]);
          if (umsg.length()>0){
            String hp = "<p>";
            String chp = "</p>";
            String nmsg = hp + umsg + chp;
            Pmsg += nmsg;
          }
    }
 server.send(200, "text/html", Pmsg );
	     Serial.println("Arg Count Recieved" );
       Serial.println(server.args());
              Serial.println("Pulling GET Data");
         for (int i = 0; i < server.args(); i++) {
              Serial.println (server.arg(i));
                  if (server.arg(i)){
                  upd_Msgs[i+1] = server.arg(i); 
                 
                    }
                  }
    Serial.println("Outputting received msgs"); 
         // for (int i = 0; i < server.args(); i++) {
             for (int i = 0; i < 20; i++) {
           Serial.println("Recvd Message ");
           Serial.println(i);
           Serial.println(upd_Msgs[i]);         
          }
          
   Serial.println("Filtering on Updated:");
  // temp_Msgs[0] = ""; // reserving first line
      for (int i = 1; i < 20; i++) {
          String msg = upd_Msgs[i];
                   // Serial.println(upd_Msgs[i]);
                   msg.toUpperCase();
          if (msg.length()>0){
                    Serial.println(upd_Msgs[i]);
                    temp_Msgs[i]=(" " + upd_Msgs[i]);
                     temp_Msgs[i].toUpperCase();
                Serial.println("Temp Updated - ");
                Serial.println(temp_Msgs[i]);
                 } 
                  else {
                         Serial.println(upd_Msgs[i]);                
                         temp_Msgs[i]= " " + Messages[i];
                    Serial.println("Temp ORIG");
                    Serial.println(temp_Msgs[i]);
                      }
           }
        Serial.println("Outputting Templist");
       
        for (int i=0; i<20; i++){
			Serial.println(i);
            Serial.println( temp_Msgs[i]);
            }
            File file = SPIFFS.open("/messages.txt", "w");
              if (!file) {
                Serial.println("Error opening file for writing");
                return;
              }
               bytesWritten = file.print("\n");
               if (bytesWritten == 0) {
                Serial.println("File write failed");
                return;
              }
             
              file.close();
    File fileToAppend = SPIFFS.open("/messages.txt", "a");
  
    if(!fileToAppend){
        Serial.println("There was an error opening the file for appending");
        return;
    }
        for (int i=0; i<20; i++){
          String appendmsg = temp_Msgs[i];
          Serial.println("appending msg");
          if(fileToAppend.println(appendmsg)){
            Serial.println(appendmsg);
            Serial.println("File content was appended");
          } else {
              Serial.println("File append failed");
          }
        }
    }

                    Messaging();
                    notifymsgupd();

                    return;
}			

void handle_Nightmode(){
     Serial.println("Night in the City!");
     server.send(200, "text/html", nightModeHTML()); 
  }
void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

//Stageupdate
void handle_stgupdate () {
   //server.argName(i);     //Get the name of the parameter
      //server.arg(i) ;              //Get the value of the parameter
  Serial.println("Current Web Data if any");     
  Serial.println(server.arg(0)); // 
  if ((stgupdate1)||(stgupdate2)||(stgupdate3)||(stgupdate4)||(stgupdate5)||(stgupdate6)||(stgupdate7)||(stgupdate8)||(stgupdate9)||(stgupdate10)||(stgupdate11)||(stgupdate12)||(stgupdate13)||(stgupdate14)||(stgupdate15)||(stgupdate16)||(stgupdate17)||(stgupdate18)||(stgupdate19));
  {
    String Pmsg ="<h1>Staging Messages being updated!</h1> <p> Info successfully received</p>\n";
    Pmsg += "<br><p align=\"left\"> <a href=\"/\" align=\"left\" >Messages</a>  </p><p align=\"left\"> <a href=\"/stage\" align=\"left\" >Stage Messages</a> </p><br>";
     
    for (int i = 0; i < server.args(); i++) {
          String smsg = server.arg(i);
                        // Serial.println(upd_sMsgs[i]);
          if (smsg.length()>0){
            String hp = "<p>";
            String chp = "</p>";
            String nmsg = hp + smsg + chp;
            Pmsg += nmsg;
            }
     }
         
 server.send(200, "text/html", Pmsg );
       Serial.println("Arg Count Recieved" );
       Serial.println(server.args());
              Serial.println("Pulling GET Data");
         for (int i = 0; i < server.args(); i++) {
              Serial.println (server.arg(i));
                  if (server.arg(i)){
                  upd_sMsgs[i+1] = server.arg(i); 
                 
                    }
                  }
    Serial.println("Outputting received msgs"); 
         // for (int i = 0; i < server.args(); i++) {
             for (int i = 0; i < 18; i++) {
           Serial.println("Recvd Message ");
           Serial.println(i);
           Serial.println(upd_sMsgs[i]);         
          }
          
   Serial.println("Filtering on stgupdated:");
  // temp_sMsgs[0] = ""; // reserving first line
      for (int i = 1; i < 18; i++) {
          String msg = upd_sMsgs[i];
                   // Serial.println(upd_sMsgs[i]);
                   
          if (msg.length()>0){
                    Serial.println(upd_sMsgs[i]);
                    temp_sMsgs[i]=(" " + upd_sMsgs[i]);
                 temp_sMsgs[i].toUpperCase();
                Serial.print ("Temp stgupdated - ");
                Serial.println(temp_sMsgs[i]);
                 } 
                  else {
                         Serial.println(upd_sMsgs[i]);                
                         temp_sMsgs[i]= " " + Stage[i];
                    Serial.println("Temp ORIG");
                    Serial.println(temp_sMsgs[i]);
                      }
           }
        Serial.println("Outputting Templist");
       
        for (int i=0; i<18; i++){
      Serial.println(i);
            Serial.println(temp_sMsgs[i]);
            }
            File file = SPIFFS.open("/Stage.txt", "w");
             
              if (!file) {
                Serial.println("Error opening file for writing");
                return;
              }
             
               bytesWritten = file.print("\n");
             
              if (bytesWritten == 0) {
                Serial.println("File write failed");
                return;
              }
             
              file.close();
    File fileToAppend = SPIFFS.open("/Stage.txt", "a");
  
    if(!fileToAppend){
        Serial.println("There was an error opening the file for appending");
        return;
    }
        for (int i=0; i<18; i++){
          String appendmsg = temp_sMsgs[i];
          Serial.println("appending msg");
          if(fileToAppend.println(appendmsg)){
            Serial.println(appendmsg);
            Serial.println("File content was appended");
          } else {
              Serial.println("File append failed");
          }
        }
    }
    Staging();
    notifymsgupd();
    return;
} 
//CStageupdate

//Commit
void handle_commit(){
   //server.argName(i);     //Get the name of the parameter
      //server.arg(i) ;              //Get the value of the parameter
  
    String Pmsg = "<h1>Commiting Staged Messages! </h1><br><p align=\"left\"> <a href=\"/\" align=\"left\" >Messages</a>  </p><p align=\"left\"> <a href=\"/stage\" align=\"left\" >Stage Messages</a> </p><br>\n";
    server.send(200, "text/html", Pmsg );
  
 //Commit 17 add 18 19 
        upd_sMsgs[0] = Messages[0];
         for (int i=1; i<18 
         ; i++) {
                  upd_sMsgs[i] = Stage[i]; 
                }
        upd_sMsgs[18] = Messages[18];    
		upd_sMsgs[19] = Messages[19];   
  //  Serial.println("Outputting committed msgs"); 
         // for (int i = 0; i < server.args(); i++) {
          for (int i = 0; i < 20; i++) {
         //  Serial.println("Recvd commit Message ");
         //  Serial.println(i);
        //   Serial.println(upd_sMsgs[i]);         
         //
           }
     File file = SPIFFS.open("/messages.txt", "w");
          if (!file) {
                Serial.println("Error opening file for writing");
                return;
            }
       
               bytesWritten = file.print("\n");
             
              if (bytesWritten == 0) {
                Serial.println("File write failed");
                return;
              }
             
              file.close();
              
    File fileToAppend = SPIFFS.open("/messages.txt", "a");
  
    if(!fileToAppend){
        Serial.println("There was an error opening the file for appending");
        return;
       }
        for (int i=0; i<20; i++){
          String appendmsg = upd_sMsgs[i];
          Serial.println("appending msg");
          if(fileToAppend.println(appendmsg)){
            Serial.println(appendmsg);
            Serial.println("File content was appended");
          } else {
              Serial.println("File append failed");
          }
        }
   
    Messaging();
    notifymsgupd();
   
  return;
} 

//Close Commit

 void handle_SETnight () {
    // converting Strings to Ints
    Serial.println("SETnight HTML status");
    // Serial.println(server.arg(0));Serial.println(server.arg(1));Serial.println(server.arg(2));
    String sShut = server.arg(0);
    long iShut = sShut.toInt();
     nightarr[0] = sShut.toInt();
 //   Shut = iShut;
    String sResume = server.arg(1);
    long iResume = sResume.toInt();
     nightarr[1] = sResume.toInt();
   // Resume = iResume;
      Serial.println("Setnight");
      Serial.println("ishut");
      Serial.println(iShut);
      Serial.println("iResume");
      Serial.println(iResume);
      //Serial.print("Night");
     // Serial.println(server.arg(0));
      String sNight= server.arg(2);
      nightarr[1] = sResume.toInt();
      Night = server.arg(2);
     if (Night == 0 || Night == false)
		 {bNight = false;}
     else 
	   {bNight = true;}
     Serial.println("Night");
     Serial.println(Night);
     Serial.println("bNight");
     Serial.println(bNight);
      
      String Nmsg ="<h1>Nightmode update</h1> <p> Info successfully received</p>";
       Nmsg += "<p align=\"left\"> <a href=\"/\" align=\"left\" >Messages</a>  </p><p align=\"left\"> <a href=\"/stage\" align=\"left\" >Staging</a>  </p>\n";

      Nmsg += sShut  + "\n" + sResume + "\n" + bNight;
     server.send(200, "text/html", Nmsg );
       File file = SPIFFS.open("/config.txt", "w");
             
              if (!file) {
                Serial.println("Error opening file for writing");
                return;
              }
             
               bytesWritten = file.print("\n");
             
              if (bytesWritten == 0) {
                Serial.println("File write failed");
                return;
              }
             
              file.close();
    File fileToAppend = SPIFFS.open("/config.txt", "a");
  
    if(!fileToAppend){
        Serial.println("There was an error opening the file for appending");
        return;
    }
        for (int i=0; i<3; i++){
          String appendmsg = server.arg(i);
          Serial.println("appending msg");
          if(fileToAppend.println(appendmsg)){
            Serial.println(appendmsg);
            Serial.println("File content was appended");
          } else {
              Serial.println("File append failed");
          }
        }
     getNightlight();
    return ;
}


//Sleepmode / Nightmode
bool getNightlight () {
  
  //if (currentHour >= 22 || currentHour <= 6) {
									 
  Serial.println("==getNightlight==");
  Serial.println("bNight (initial F)");
										 
						  
  Serial.println(bNight);
											 

  if ((bNight == false)&&(nightarr[1] == 0) ){
      nightarr[0] = currentHour-1;
     }
  Serial.print("ishut");
  Serial.println(nightarr[0]);
  Serial.print("iresume");
  Serial.println(nightarr[1]);
      if (bNight == true) { 
     int Shut= nightarr[0];
     int Resume = nightarr[1];
        if (currentHour >= Shut || currentHour < nightarr[1]) {
               Serial.println("==getNightlight==");
               Serial.print("...Nightmode Active...");
               Serial.println();
               Serial.println(currentHour); 
               return true;
               } else {
                return false;
               }
    } else {
  Serial.println("==getNightlight==");
  Serial.println("...DayMode...");
  Serial.println("currentHour"); 
  Serial.println(currentHour); 
  Serial.print("ishut");
  Serial.println(nightarr[0]);
  Serial.print("iresume");
  Serial.println(nightarr[1]);
   return false;
   }
}


void dtime () {
  
    String tmsg = "***            ";
    String tcmsg = "             ***";
    String tcol = ":";
    tmsg += currentHour + tcol + currentMinute + tcmsg + currentHour + tcol + currentMinute + tcmsg;
    Serial.println(tmsg);
   //   int specmsg = (tmsg.length() - 1);
   for (int i=0; i<tmsg.length(); i++){
  
  // scroll down display
  displaybuffer[0] = displaybuffer[1];
  displaybuffer[1] = displaybuffer[2];
  displaybuffer[2] = displaybuffer[3];
  displaybuffer[3] = displaybuffer[4];
  displaybuffer[4] = displaybuffer[5];
  displaybuffer[5] = displaybuffer[6];
  displaybuffer[6] = displaybuffer[7];
  displaybuffer[7] = displaybuffer[8];
  displaybuffer[8] = displaybuffer[9];
  displaybuffer[9] = displaybuffer[10];
  displaybuffer[10] = displaybuffer[11];
  displaybuffer[11] = tmsg.charAt(i);

  // set every digit to the buffer
  disp[0].alpha4.writeDigitAscii(0, displaybuffer[0]);
  disp[0].alpha4.writeDigitAscii(1, displaybuffer[1]);
  disp[0].alpha4.writeDigitAscii(2, displaybuffer[2]);
  disp[0].alpha4.writeDigitAscii(3, displaybuffer[3]);
  disp[1].alpha4.writeDigitAscii(0, displaybuffer[4]);
  disp[1].alpha4.writeDigitAscii(1, displaybuffer[5]);
  disp[1].alpha4.writeDigitAscii(2, displaybuffer[6]);
  disp[1].alpha4.writeDigitAscii(3, displaybuffer[7]);
  disp[2].alpha4.writeDigitAscii(0, displaybuffer[8]);
  disp[2].alpha4.writeDigitAscii(1, displaybuffer[9]);
  disp[2].alpha4.writeDigitAscii(2, displaybuffer[10]);
  disp[2].alpha4.writeDigitAscii(3, displaybuffer[11]);
 
  // write it out!
  disp[0].alpha4.writeDisplay();
  disp[1].alpha4.writeDisplay();
  disp[2].alpha4.writeDisplay();
  delay(150);
  }
  return;
}


void loop(void) {
 
timeNow = millis()/1000; // the number of milliseconds that have passed since boot
seconds = timeNow - timeLast;//the number of seconds that have passed since the last time 60 seconds was reached.

Serial.println("==Void Loop==");
Serial.println("VLcurrentHour");
Serial.println(currentHour);
Serial.println("VLcurrentMinute");
Serial.println(currentMinute);
Serial.println("Millis hours");
Serial.println(hours);
Serial.println("Millis minutes");
Serial.println(minutes);
Serial.println("Millis seconds");
Serial.println(seconds);

if (seconds >= 60) {
  Serial.println("-Seconds to minutes-");
  if (minutes==0){
    currentMinute=currentMinute+2;
  }
  timeLast = timeNow;
  minutes = minutes + 1;
  currentMinute = currentMinute+1;
  Serial.println("-Minutes-");
  Serial.println(minutes);
  Serial.println("-currentMinute-");
  Serial.println(currentMinute);
  }
//if one minute has passed, start counting milliseconds from zero again and add one minute to the clock.

if (currentMinute >= 60){ 
    Serial.println("-Minutes to hours-");
  minutes = 0;
  currentMinute =0;
  hours = hours + 1;
  currentHour = currentHour+1;
  Serial.println("-Hour-");
  Serial.println(hours);
  Serial.println("currentHour");
  Serial.println(currentHour);
  }

// if one hour has passed, start counting minutes from zero and add one hour to the clock

if (hours >= 1){
   getTime();
  Serial.println("==1 hours has elapsed getting NTP Time==");
  Serial.println("currentHour");
  Serial.println(currentHour);
  Serial.println("==NTP Updated==");
  Serial.println("currentHour");
  Serial.println(currentHour);
  Serial.println("currentMinute");
  Serial.println(currentMinute);
  hours =0;
        }
      
  bool nightlight = getNightlight();
        Serial.println("...Nightlight status...");
        Serial.println("==Returns Boolean False 0 Active==");
        Serial.println("==Returns Boolean True 1 DeActive==");
        Serial.println(nightlight);
       if (nightlight == false) {
        Serial.println("::Mirror Active::");
        Serial.println("");
        
        server.handleClient();
          // affirmation mirror
          long state = digitalRead(sensor);
          delay(1000);
 
			if(state == HIGH){                   // remark this line out if no PIR Motion Sensor being used 
              digitalWrite (Status, HIGH);           // remark this line out if no PIR Motion Sensor being used 
              Serial.println("Motion Detected!");     // remark this line out if no PIR Motion Sensor being used 
              String displayRandomMessage = Messages[random(1,17)];
              Serial.println(displayRandomMessage);
        
              for (int i=0; i<displayRandomMessage.length(); i++){
                
                // scroll down display
                displaybuffer[0] = displaybuffer[1];
                displaybuffer[1] = displaybuffer[2];
                displaybuffer[2] = displaybuffer[3];
                displaybuffer[3] = displaybuffer[4];
                displaybuffer[4] = displaybuffer[5];
                displaybuffer[5] = displaybuffer[6];
                displaybuffer[6] = displaybuffer[7];
                displaybuffer[7] = displaybuffer[8];
                displaybuffer[8] = displaybuffer[9];
                displaybuffer[9] = displaybuffer[10];
                displaybuffer[10] = displaybuffer[11];
                displaybuffer[11] = displayRandomMessage.charAt(i);
        
                // set every digit to the buffer
                disp[0].alpha4.writeDigitAscii(0, displaybuffer[0]);
                disp[0].alpha4.writeDigitAscii(1, displaybuffer[1]);
                disp[0].alpha4.writeDigitAscii(2, displaybuffer[2]);
                disp[0].alpha4.writeDigitAscii(3, displaybuffer[3]);
                disp[1].alpha4.writeDigitAscii(0, displaybuffer[4]);
                disp[1].alpha4.writeDigitAscii(1, displaybuffer[5]);
                disp[1].alpha4.writeDigitAscii(2, displaybuffer[6]);
                disp[1].alpha4.writeDigitAscii(3, displaybuffer[7]);
                disp[2].alpha4.writeDigitAscii(0, displaybuffer[8]);
                disp[2].alpha4.writeDigitAscii(1, displaybuffer[9]);
                disp[2].alpha4.writeDigitAscii(2, displaybuffer[10]);
                disp[2].alpha4.writeDigitAscii(3, displaybuffer[11]);
               
                // write it out!
                disp[0].alpha4.writeDisplay();
                disp[1].alpha4.writeDisplay();
                disp[2].alpha4.writeDisplay();
                delay(150);
                
                }

                }
                else {                                     // remark this line out if no PIR Motion Sensor being used 
                  digitalWrite (Status, LOW);               // remark this line out if no PIR Motion Sensor being used 
                  Serial.println("Motion absent!");        // remark this line out if no PIR Motion Sensor being used 
                  }                                          // remark this line out if no PIR Motion Sensor being used 

        // affirmation mirror
       } else {
        server.handleClient();
        Serial.println(" ::Mirror Deactivated:: ");
        Serial.println("");
        Serial.println("== Nightlight Returns True==");

        }
  
}
			
