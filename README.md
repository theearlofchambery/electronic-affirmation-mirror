This code adds features to the Electronic Affirmation Mirror - https://www.instructables.com/Electronic-Affirmation-Mirror/.  There is no warranty, use at your own risk. 

Equipment used for the mirror were a generic NodeMCU ESP2866 12E and PIR motion sensor bought randomly of Amazon.  The sketch was compiled and uploaded using the Arduino IDE with the esptool.py plugin from instructions found here - https://randomnerdtutorials.com/install-esp8266-filesystem-uploader-arduino-ide/. You may be able to create the messages from webpage without messing with the text file, I have not tried doing that.

Future updates:
1. Improve webpage behavior
2. Checkbox to run the list in order instead of at Random
3. Config file to save settings 
3.5 Config is opened and read - need to add Write to update if any changes are made
4. Text fun with the LEDs
